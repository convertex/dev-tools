#!/usr/bin/bash

# colours of the rainbow (just for fun)

# Reset
Color_Off='\033[0m' # Text Reset

# Regular Colors
Black='\033[0;30m'  # Black
Red='\033[0;31m'    # Red
Green='\033[0;32m'  # Green
Yellow='\033[0;33m' # Yellow
Blue='\033[0;34m'   # Blue
Purple='\033[0;35m' # Purple
Cyan='\033[0;36m'   # Cyan
White='\033[0;37m'  # White

# High Intensity
IBlack='\033[0;90m'  # Black
IRed='\033[0;91m'    # Red
IGreen='\033[0;92m'  # Green
IYellow='\033[0;93m' # Yellow
IBlue='\033[0;94m'   # Blue
IPurple='\033[0;95m' # Purple
ICyan='\033[0;96m'   # Cyan
IWhite='\033[0;97m'  # White

SEARCH_DIR=../sample_data/input_all

EORI_PATTERN_HMRC='^(GB|XI)[0-9]{12,15}$' # based on the response given by SOAP UI on a bad request
EORI_PATTERN_SPEC='^(GB|XI)[0-9]{12}$'
HMRC_ENDPOINT=https://test-api.service.hmrc.gov.uk
EORI_CHECK_URI=${HMRC_ENDPOINT}/customs/eori/lookup/check-multiple-eori
SERVER_INVALID_PAYLOAD_MESSAGE='Invalid payload'

NR_OF_PASSED=
NR_OF_ERRORS=

basic_api_check() {
  curl -s -X GET "$HMRC_ENDPOINT"/hello/world \
    -H 'Accept: application/vnd.hmrc.1.0+json' |
    jq

  curl -s -X POST "$EORI_CHECK_URI" \
    -H 'Accept: application/vnd.hmrc.1.0+json' \
    -H 'Content-Type: application/json' \
    -d '{"eoris":["GB182739473232"]}' |
    jq '.[]'

  echo valid
  curl -s -X POST $EORI_CHECK_URI \
    -H 'Accept: application/vnd.hmrc.1.0+json' \
    -H 'Content-Type: application/json' \
    -d '{"eoris":["GB182739473232","GB123456789136","GB8392848394939"]}' |
    jq '.[] | select(.valid==true) | .eori'

  echo non valid
  curl -s -X POST $EORI_CHECK_URI \
    -H 'Accept: application/vnd.hmrc.1.0+json' \
    -H 'Content-Type: application/json' \
    -d '{"eoris":["GB182739473232","GB123456789136","GB8392848394939"]}' |
    jq '.[] | select(.valid==false) | .eori'
}

query_endpoint() {
  for entry in "$SEARCH_DIR"/*; do

    inputID=$(jq '.[] | .ID' "$entry" | sed 's/"//g')
    inputEORI=$(jq '.[] | .EORI' "$entry" | sed 's/"//g')

    data='{"eoris":['\""$inputEORI"\"']}'

    #    if [ "$(grep -Ec "$EORI_PATTERN_SPEC" <<<$inputEORI)" -gt 0 ]; then
    curl -s -X POST "$EORI_CHECK_URI" \
      -H 'Accept: application/vnd.hmrc.1.0+json' \
      -H 'Content-Type: application/json' \
      -d "$data" |
      #        jq >/dev/null 2>&1
      jq
    #    fi
    exit_code=$?
    if [ $exit_code -ne 0 ]; then
      ((NR_OF_ERRORS++))
    else
      ((NR_OF_PASSED++))
    fi
  done
}

test_with_biz_validation() {
  echo -e Processing input for "$Blue"$(ls "$SEARCH_DIR" | wc -w)"$Color_Off" files...
  for entry in "$SEARCH_DIR"/*; do

    inputID=$(jq '.[] | .ID' "$entry" | sed 's/"//g')
    inputEORI=$(jq '.[] | .EORI' "$entry" | sed 's/"//g')

    data='{"eoris":['\""$inputEORI"\"']}'

    #    if [ "$(grep -Ec "$EORI_PATTERN_SPEC" <<<$inputEORI)" -gt 0 ]; then
    curl -s -X POST "$EORI_CHECK_URI" \
      -H 'Accept: application/vnd.hmrc.1.0+json' \
      -H 'Content-Type: application/json' \
      -d "$data" |
      #        jq >/dev/null 2>&1
      jq '.[] | select(.valid==false) | .eori'
    #    fi
    exit_code=$?
    if [ $exit_code -ne 0 ]; then
      ((NR_OF_ERRORS++))
      echo
      echo -e "$Red"\[ ERROR \]"$Color_Off" "$Yellow"EORI REST API verification "$Color_Off""$Red" "$inputEORI" for ID"$Color_Off" "$Yellow""$inputID""$Color_Off"
    else
      ((NR_OF_PASSED++))
    fi
  done

  echo
  echo "Report: Checking EORI (based on API call)"
  echo
  echo -e Tests run: "$IYellow"$(expr $NR_OF_PASSED + $NR_OF_ERRORS)"$Color_Off"
  echo -e Tests passed: "$Green""$NR_OF_PASSED""$Color_Off"
  echo Failures:
  echo -e Errors: "$Red""$NR_OF_ERRORS""$Color_Off"
  echo Skipped:
}

call_api() {
  inputID=$(jq '.[] | .ID' "$entry" | sed 's/"//g')
  inputEORI=$(jq '.[] | .EORI' "$entry" | sed 's/"//g')

  data='{"eoris":['\""$inputEORI"\"']}'

  FINAL='[]'

  #    if [ "$(grep -Ec "$EORI_PATTERN_SPEC" <<<$inputEORI)" -gt 0 ]; then
  #    echo '['
  RESULT=$(curl -s -X POST "$EORI_CHECK_URI" \
    -H 'Accept: application/vnd.hmrc.1.0+json' \
    -H 'Content-Type: application/json' \
    -d "$data" |
    jq '.[]')

  exit_code=$?
  if [ $exit_code -ne 0 ]; then
    ((NR_OF_ERRORS++))
    echo error
  else
    ((NR_OF_PASSED++))
  fi

  EORI_ID=$(echo "$RESULT" | jq --raw-output '.eori')
  EORI_VALID=$(echo "$RESULT" | jq --raw-output '.valid')

  echo "$EORI_ID" "$EORI_VALID"
}

save_api_response() {
  for entry in "$SEARCH_DIR"/*; do

    call_api "$entry"

    #    export FOO="bar"
    #    jq -n --arg foo "$FOO" '{foo: $foo}'

    #        jq -n --arg result "$RESULT" '[{$result}]'

    #  RESULT=$(echo '{"key":"value"}' | jq | jq '''.x += {"a":"na"}')
    #    SEMMI=$(echo '[]' | jq | jq n --arg "foo" "$RESULT" '. += $RESULT')

    #    echo "$RESULT"

    #      jq '.[] | select(.valid==true) | .[]'
    #    fi
  done

  echo
  echo "Report: Checking EORI (based on API call)"
  echo
  echo -e Tests run: "$Yellow"$(expr $NR_OF_PASSED + $NR_OF_ERRORS)"$Color_Off"
  echo -e Tests passed: "$Green""$NR_OF_PASSED""$Color_Off"
  echo Failures:
  echo -e Errors: "$Red""$NR_OF_ERRORS""$Color_Off"
  echo Skipped:

}
#basic_api_check
#query_endpoint
#test_with_biz_validation
save_api_response
