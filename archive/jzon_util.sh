#!/usr/bin/bash

count=$(jq '.[0] | keys | length' sample_data/input/1NPJN7MYWA7.json)

for ((i = 0; i < $count; i++)); do

  key=$(jq -r '.[0] | keys | .['"$i"']' sample_data/input/1NPJN7MYWA7.json)
  #  echo "$key"

  if [ "$(grep -Ec "^[A-Z \d\W]+$" <<<$key)" -le 0 ]; then
    java_global_constant=$(sed 's/[[:upper:]]/_\L&/g' <<<$(sed 's/^./\L&/g' <<<"$key") | tr '[:lower:]' '[:upper:]')
    echo "public static final String" "$java_global_constant" \= "\"""$key""\";"
  else
    java_global_constant="$key"
    echo "public static final String" "$java_global_constant" \= "\"""$key""\";"
  fi
  echo
done
