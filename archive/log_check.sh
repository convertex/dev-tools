#!/usr/bin/bash
# Reset
Color_Off='\033[0m' # Text Reset

# Regular Colors
Black='\033[0;30m'  # Black
Red='\033[0;31m'    # Red
Green='\033[0;32m'  # Green
Yellow='\033[0;33m' # Yellow
Blue='\033[0;34m'   # Blue
Purple='\033[0;35m' # Purple
Cyan='\033[0;36m'   # Cyan
White='\033[0;37m'  # White

QA_CHK_LOG_PATH=../testware/log/
QA_CHK_LOG_FULL_PATH="$QA_CHK_LOG_PATH"/conversion_quality_check.log

TESTWARE_DIR=../testware

INPUT_DIR=../sample_data/input_all
OUTPUT_DIR=../sample_data/output_all

ioInfo() {
  echo
  echo -e Processing log...
  echo
  echo -e Number of input "$Blue"$(ls "$INPUT_DIR" | wc -w)"$Color_Off" flat file\(s\)...
  echo -e Number of output "$Blue"$(ls "$OUTPUT_DIR" | wc -w)"$Color_Off" flat file\(s\)...
}

testNumberOfFieldsInOutput() {

  ALL_OUTPUT_FILES=$(ls "$OUTPUT_DIR" | wc -w)

  CHK_FILE="$(grep -E -i -w 'CHK_FILE' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  #  erronous file name
  ERROR_FILE_NAME="$(grep -E -i -w 'ERROR_FILE_NAME' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  # number of fields in the pattern OK rows
  CHK_NUMBER_OF_FIELDS="$(grep -E -i -w 'CHK_NUMBER_OF_FIELDS' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  #  number of rows where the specified number of fields found
  CHK_PASSED="$(grep -E -i -w 'CHK_PASSED' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  # number of rows the expected (based on spec) and the actual number of fields are different
  FAILURE_NUMBER_OF_FIELDS="$(grep -E -i -w 'FAILURE_NUMBER_OF_FIELDS' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  ERROR_ROW_PATTERN="$(grep -E -i -w 'ERROR_ROW_PATTERN' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  ERROR_ROW_MISSING="$(grep -E -i -w 'ERROR_ROW_MISSING' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  ERROR_FIELD_MISSING="$(grep -E -i -w 'ERROR_FIELD_MISSING' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  echo
  echo Report: Checking number of fields in row
  echo
  echo -e All files in output:'     '"$Blue""$ALL_OUTPUT_FILES""$Color_Off"
  echo
  echo -e Passed file name pattern: "$Green""$CHK_FILE""$Color_Off"
  echo -e Errors file name pattern: "$Red"$(expr $ALL_OUTPUT_FILES - $CHK_FILE)"$Color_Off"
  echo
  echo -e Tests run: "$Blue"$(expr $CHK_PASSED + $FAILURE_NUMBER_OF_FIELDS + $ERROR_ROW_PATTERN)"$Color_Off"

  echo -e Tests passed: "$Green""$CHK_PASSED""$Color_Off"
  echo -e Failures: "$Yellow""$FAILURE_NUMBER_OF_FIELDS""$Color_Off"
  echo -e Errors \(row sysntax\): "$Red""$ERROR_ROW_PATTERN""$Color_Off"

  #  echo -e Errors missing row: "$Red""$ERROR_ROW_MISSING""$Color_Off"
  #  echo -e Errors missing field: "$Red""$ERROR_FIELD_MISSING""$Color_Off"

  echo Skipped: \-
}

testMapping() {
  # all files in the sample output directory

  CHK_FILE_NAME_JZON="$(grep -E -i -w 'CHK_FILE_NAME_JZON' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  CHK_FILE_NAME_FLAT="$(grep -E -i -w 'CHK_FILE_NAME_FLAT' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  CHK_FILE="$(grep -E -i -w 'CHK_FILE' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  #  erronous file name
  ERROR_FILE_NAME_PATTERN_JZON="$(grep -E -i -w 'ERROR_FILE_NAME_PATTERN_JZON' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  ERROR_FILE_NAME_PATTERN_FLAT="$(grep -E -i -w 'ERROR_FILE_NAME_PATTERN_FLAT' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  ERROR_FILE_NAME_PAIR="$(grep -E -i -w 'ERROR_FILE_NAME_PAIR' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  ERROR_UNRECOGNIZED_OUTPUT_FIELD="$(grep -E -i -w 'ERROR_UNRECOGNIZED_OUTPUT_FIELD' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  ERROR_ROW_PATTERN="$(grep -E -i -w 'ERROR_ROW_PATTERN' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  ERROR_CHK_MAPPING_MISSING_SRC_VALUE="$(grep -E -i -w 'ERROR_CHK_MAPPING_MISSING_SRC_VALUE' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  CHK_MAPPING_FIELD="$(grep -E -i -w 'CHK_MAPPING_FIELD' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  FAILURE_MAPPING="$(grep -E -i -w 'FAILURE_MAPPING' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  ioInfo

  echo
  echo -e Passed input file name pattern:'     '"$Green""$CHK_FILE_NAME_JZON""$Color_Off"
  echo -e Passed output file name pattern:'    '"$Green""$CHK_FILE_NAME_FLAT""$Color_Off"
  echo
  echo -e Passed number file of mapping:'      '"$Green""$CHK_FILE""$Color_Off"
  echo
  echo -e Errors file name pattern \(jzon\):'   '"$Red""$ERROR_FILE_NAME_PATTERN_JZON""$Color_Off"
  echo -e Errors file name pattern \(flat\):'   '"$Red""$ERROR_FILE_NAME_PATTERN_FLAT""$Color_Off"
  echo
  echo -e Errors file pairing:'                '"$Red""$ERROR_FILE_NAME_PAIR""$Color_Off"
  echo -e Errors identifying flat file field:' '"$Red""$ERROR_UNRECOGNIZED_OUTPUT_FIELD""$Color_Off"
  echo -e Errors row pattern:'                 '"$Red""$ERROR_ROW_PATTERN""$Color_Off"
  echo
  echo -e Passed number of mapping:'           '"$Green""$CHK_MAPPING_FIELD""$Color_Off"
  echo -e Failure number of mapping:'          '"$Yellow""$FAILURE_MAPPING""$Color_Off"
  echo
  echo -e Errors mapping missing jzon field:'  '"$Red""$ERROR_MAPPING_MISSING_SRC_VALUE""$Color_Off"
}

testCorrectJzonFileNames() {
  CHK_FILE_NAME_JZON="$(grep -E -i -w 'CHK_FILE_NAME_JZON' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  ERROR_FILE_NAME_PATTERN_JZON="$(grep -E -i -w 'ERROR_FILE_NAME_PATTERN_JZON' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  echo
  echo Processing input file dir
  echo
  echo -e Number of input'           '"$Blue"$(ls "$INPUT_DIR" | wc -w)"$Color_Off"
  echo -e Passed file name pattern:' '"$Green""$CHK_FILE_NAME_JZON""$Color_Off"
  echo -e Errors file name pattern:' '"$Red""$ERROR_FILE_NAME_PATTERN_JZON""$Color_Off"
}

testCorrectFlatFileNames() {
  CHK_FILE_NAME_FLAT="$(grep -E -i -w 'CHK_FILE_NAME_FLAT' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  ERROR_FILE_NAME_PATTERN_FLAT="$(grep -E -i -w 'ERROR_FILE_NAME_PATTERN_FLAT' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  echo
  echo Processing output file dir
  echo
  echo -e Number of output'          '"$Blue"$(ls "$OUTPUT_DIR" | wc -w)"$Color_Off"
  echo -e Passed file name pattern:' '"$Green""$CHK_FILE_NAME_FLAT""$Color_Off"
  echo -e Errors file name pattern:' '"$Red""$ERROR_FILE_NAME_PATTERN_FLAT""$Color_Off"
}

testFileNameAgainstID() {
  CHK_FILE_NAME_TO_ID="$(grep -E -i -w 'CHK_FILE_NAME_TO_ID' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  FAILURE_FILE_NAME_TO_ID="$(grep -E -i -w 'FAILURE_FILE_NAME_TO_ID' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  ERROR_FILE_NAME_TO_ID="$(grep -E -i -w 'ERROR_FILE_NAME_TO_ID' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  echo
  echo Checking out file name containig its ID
  echo
  echo -e Number of output'        '"$Blue"$(ls "$OUTPUT_DIR" | wc -w)"$Color_Off"
  echo -e Passed ID in file name:' '"$Green""$CHK_FILE_NAME_TO_ID""$Color_Off"
  echo -e Errors ID in file name:' '"$Yellow""FAILURE_FILE_NAME_TO_ID""$Color_Off"
  echo -e Errors ID in file name:' '"$Red""$ERROR_FILE_NAME_TO_ID""$Color_Off"
}

#grep -E -i '\[ ' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l
#grep -E -i ' \]' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l

RAW_DATA=raw_data.dat
RAW_DATA_CSV=raw_data.csv
grep -E -i '\[.*\]' <<<$(cat "$QA_CHK_LOG_FULL_PATH") > "$RAW_DATA"

#
#FLAT_LINE='[ TEC ] [ CHK_MAPPING_FIELDS_TARGET_ONLY ] [ + ] [ EXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER ] '
#
#echo "$FLAT_LINE" | grep -E -i '\]'
#echo "$FLAT_LINE" | grep -E -i '\['
#
cat "$RAW_DATA" | sed 's/\[ + ]//' | sed -E 's/ *\]/;/g' | sed -E 's/\[ *//g' > "$RAW_DATA_CSV"



#echo
#echo Check file name patterns...
#echo
#testCorrectJzonFileNames
#testCorrectFlatFileNames

#PS3="Specify the report type: "
#items=("testNumberOfFieldsInOutput" "testMapping" "testFileNameAgainstID")
#select item in "${items[@]}" Quit; do
#  case $REPLY in
#  1)
##    here_am_i=`pwd`
##    cd "$TESTWARE_DIR" && mvn spring-boot:run -D"spring-boot.run.arguments=-c"
##    cd $here_am_i
#    testNumberOfFieldsInOutput
#    exit
#    ;;
#  2)
#    testMapping
#    exit
#    ;;
#  3)
#    testFileNameAgainstID
#    exit
#    ;;
#  $((${#items[@]} + 1))) break ;;
#  esac
#done
