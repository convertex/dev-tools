#!/usr/bin/bash

# Reset
Color_Off='\033[0m' # Text Reset

# Regular Colors
Black='\033[0;30m'  # Black
Red='\033[0;31m'    # Red
Green='\033[0;32m'  # Green
Yellow='\033[0;33m' # Yellow
Blue='\033[0;34m'   # Blue
Purple='\033[0;35m' # Purple
Cyan='\033[0;36m'   # Cyan
White='\033[0;37m'  # White

PROJECT_DIR=../testware

QA_CHK_LOG_PATH="$PROJECT_DIR"/log/
QA_CHK_LOG_FULL_PATH="$QA_CHK_LOG_PATH"/conversion_quality_check.log

config_file="$PROJECT_DIR"/src/main/resources/config.properties
function prop {
  grep "${1}" ${config_file} | cut -d'=' -f2
}

INPUT_DIR=$(prop '^data.sample.input=')
OUTPUT_DIR=$(prop '^data.sample.output=')

SAMPLE_ROOT="$(
  cd $INPUT_DIR
  cd ../
  pwd
)"

TMP_SAMPLE="$SAMPLE_ROOT"/samples.json

echo -e "\n$(cat $INPUT_DIR/$1)\n\n\n" > "$TMP_SAMPLE"
echo "$(cat $OUTPUT_DIR/$2)" >> "$TMP_SAMPLE"

nano $TMP_SAMPLE

echo
rm -f "$TMP_SAMPLE" && echo -e "The temporary file $Yellow$TMP_SAMPLE$Color_Off$Green has been removed$Color_Off."
echo
