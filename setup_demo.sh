#!/usr/bin/bash
# Reset
Color_Off='\033[0m' # Text Reset

# Regular Colors
Black='\033[0;30m'  # Black
Red='\033[0;31m'    # Red
Green='\033[0;32m'  # Green
Yellow='\033[0;33m' # Yellow
Blue='\033[0;34m'   # Blue
Purple='\033[0;35m' # Purple
Cyan='\033[0;36m'   # Cyan
White='\033[0;37m'  # White

display_blue() {
  printf "$2%-1s$Blue$1$Color_Off\n"
}

display_green() {
  printf "$2%-1s$Green$1$Color_Off\n"
}

display_yellow() {
  printf "$2%-1s$Yellow$1$Color_Off\n"
}

display_red() {
  printf "$2%-1s$Red$1$Color_Off\n"
}

QA_CHK_LOG_PATH=../testware/log/
QA_CHK_LOG_FULL_PATH="$QA_CHK_LOG_PATH"/conversion_quality_check.log

TESTWARE_DIR=../testware

INPUT_DIR_ALL=../sample_data/input_all
OUTPUT_DIR_ALL=../sample_data/output_all

INPUT_DIR_DEMO=../sample_data/input
OUTPUT_DIR_DEMO=../sample_data/output

sync_by_demo_input() {
  INPUT_DEMO_FILES="$(find "$INPUT_DIR_DEMO" -type f | cut -d'/' -f4 | cut -d'.' -f1)"

  echo
  echo -e Syncing demo "$Yellow"output"$Color_Off" based on demo input...
  echo
  for demo_input_file in $INPUT_DEMO_FILES; do
    sample_output_file="$(ls $OUTPUT_DIR_ALL | grep -e "$demo_input_file")"

    display_green "$sample_output_file" "$demo_input_file"

    if [ ! -z "$sample_output_file" ]; then
      cp "$OUTPUT_DIR_ALL"/"$sample_output_file" "$OUTPUT_DIR_DEMO"
    fi

  done
}

sync_by_demo_output() {
  OUTPUT_DEMO_FILES="$(find "$OUTPUT_DIR_DEMO" -type f | cut -d'/' -f4 | cut -d'_' -f1)"

  echo
  echo -e Syncing demo "$Blue"input"$Color_Off" based on demo output...
  echo
  for demo_output_file in $OUTPUT_DEMO_FILES; do
    sample_input_file="$(ls $INPUT_DIR_ALL | grep -e "$demo_output_file")"

    display_green "$sample_input_file" "$demo_output_file"

    if [ ! -z "$sample_input_file" ]; then
      cp "$INPUT_DIR_ALL"/"$sample_input_file" "$INPUT_DIR_DEMO"
    fi

  done
}
synchronize_all() {
  sync_by_demo_input
  sync_by_demo_output
}

echo
PS3="Specify the sync type: "
items=("sync_by_demo_input" "sync_by_demo_output" "synchronize_all")
select item in "${items[@]}" Quit; do
  case $REPLY in
  1)
    sync_by_demo_input
    exit
    ;;
  2)
    sync_by_demo_output
    exit
    ;;
  3)
    synchronize_all
    exit
    ;;
  $((${#items[@]} + 1))) break ;;
  esac
done

echo
echo Directories have been synchronized

