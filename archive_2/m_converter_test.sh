#!/usr/bin/bash
# Reset
Color_Off='\033[0m' # Text Reset

# Regular Colors
Black='\033[0;30m'  # Black
Red='\033[0;31m'    # Red
Green='\033[0;32m'  # Green
Yellow='\033[0;33m' # Yellow
Blue='\033[0;34m'   # Blue
Purple='\033[0;35m' # Purple
Cyan='\033[0;36m'   # Cyan
White='\033[0;37m'  # White

PROJECT_DIR=../testware

QA_CHK_LOG_PATH="$PROJECT_DIR"/log/
QA_CHK_LOG_FULL_PATH="$QA_CHK_LOG_PATH"/conversion_quality_check.log

config_file="$PROJECT_DIR"/src/main/resources/config.properties
function prop {
  grep "${1}" ${config_file} | cut -d'=' -f2
}

INPUT_DIR=$(prop '^data.sample.input=')
OUTPUT_DIR=$(prop '^data.sample.output=')

NUMBER_OF_INPUT="$(ls $INPUT_DIR | wc -w)"
NUMBER_OF_OUTPUT="$(ls $OUTPUT_DIR | wc -w)"

ioInfo() {
  echo
  echo -e Processing log...
  echo
  echo -e Number of input "$Blue""$NUMBER_OF_INPUT""$Color_Off" flat file\(s\)...
  echo -e Number of output "$Blue""$NUMBER_OF_OUTPUT""$Color_Off" flat file\(s\)...
}

display_blue() {
  printf "$2%-1s$Blue$1$Color_Off\n"
}

display_green() {
  printf "$2%-1s$Green$1$Color_Off\n"
}

display_yellow() {
  printf "$2%-1s$Yellow$1$Color_Off\n"
}

display_red() {
  printf "$2%-1s$Red$1$Color_Off\n"
}

testMapping_calculation() {

  ioInfo

  echo

  CHK_FILE_NAME_JZON="$(grep -E -w -c 'CHK_FILE_NAME_JZON' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_green "$CHK_FILE_NAME_JZON" 'CHK_FILE_NAME_JZON: '

  ERROR_FILE_NAME_PATTERN_JZON="$(grep -E -w -c 'ERROR_FILE_NAME_PATTERN_JZON' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_red "$ERROR_FILE_NAME_PATTERN_JZON" 'ERROR_FILE_NAME_PATTERN_JZON: '
  echo

  CHK_FILE_NAME_FLAT="$(grep -E -w -c 'CHK_FILE_NAME_FLAT' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_green "$CHK_FILE_NAME_FLAT" 'CHK_FILE_NAME_FLAT: '

  ERROR_FILE_NAME_PATTERN_FLAT="$(grep -E -w -c 'ERROR_FILE_NAME_PATTERN_FLAT' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_red "$ERROR_FILE_NAME_PATTERN_FLAT" 'ERROR_FILE_NAME_PATTERN_FLAT: '
  echo
#
#  CHK_NUMBER_OF_FIELDS="$(grep -E -w -c 'CHK_NUMBER_OF_FIELDS' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
#  display_green "$CHK_NUMBER_OF_FIELDS" 'CHK_NUMBER_OF_FIELDS: '
#
#  FAILURE_NUMBER_OF_FIELDS="$(grep -E -w -c 'FAILURE_NUMBER_OF_FIELDS' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
#  display_yellow "$FAILURE_NUMBER_OF_FIELDS" 'FAILURE_NUMBER_OF_FIELDS: '
#  echo

  CHK_FILE="$(grep -E -w -c 'CHK_FILE' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_green "$CHK_FILE" 'CHK_FILE: '
  echo

  echo Comparing input and output fields...
  echo
  ERROR_ROW_PATTERN="$(grep -E -w -c 'ERROR_ROW_PATTERN' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_red "$ERROR_ROW_PATTERN" '+ ERROR_ROW_PATTERN: '
  echo

  CHK_MAPPING_FIELDS="$(grep -E -w -c 'CHK_MAPPING_FIELDS' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_green "$CHK_MAPPING_FIELDS" '+ CHK_MAPPING_FIELDS: '

  CHK_MAPPING_FIELDS_TARGET_ONLY="$(grep -E -w -c 'CHK_MAPPING_FIELDS_TARGET_ONLY' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_green "$CHK_MAPPING_FIELDS_TARGET_ONLY" '+ CHK_MAPPING_FIELDS_TARGET_ONLY: '

  EXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER="$(grep -E -w -c 'EXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_green "$EXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER" 'D EXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER: '

  CHK_MAPPING_FIELDS_USER_TIME_NULL="$(grep -E -w -c 'CHK_MAPPING_FIELDS_USER_TIME_NULL' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_green "$CHK_MAPPING_FIELDS_USER_TIME_NULL" '+ CHK_MAPPING_FIELDS_USER_TIME_NULL: '

  FAILURE_MAPPING="$(grep -E -w -c 'FAILURE_MAPPING' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_yellow "$FAILURE_MAPPING" '+ FAILURE_MAPPING: '

  UNEXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER="$(grep -E -w -c 'UNEXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_red "$UNEXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER" 'D UNEXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER: '

  ERROR_MAPPING_MISSING_SRC_VALUE="$(grep -E -w -c 'ERROR_MAPPING_MISSING_SRC_VALUE' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_red "$ERROR_MAPPING_MISSING_SRC_VALUE" '+ ERROR_MAPPING_MISSING_SRC_VALUE: '
  echo

  ERROR_MAPPING_UNDEFINED="$(grep -E -w -c 'ERROR_MAPPING_UNDEFINED' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_red "$ERROR_MAPPING_UNDEFINED" '+ ERROR_MAPPING_UNDEFINED: '

  ERROR_MAPPING_MISSING_TRG_VALUE="$(grep -E -w -c 'ERROR_MAPPING_MISSING_TRG_VALUE' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_red "$ERROR_MAPPING_MISSING_TRG_VALUE" '+ ERROR_MAPPING_MISSING_TRG_VALUE: '
  echo

  #  STR_COUNT="$(grep -E -w -c '\[ STR \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  #  display_blue "$STR_COUNT" 'STR_COUNT: '
  #
  #  GEN_COUNT="$(grep -E -w -c '\[ GEN \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  #  display_blue "$GEN_COUNT" 'GEN_COUNT: '
  #
  #  TEC_COUNT="$(grep -E -w -c '\[ TEC \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  #  display_blue "$TEC_COUNT" 'TEC_COUNT: '
  #
  #  DET_COUNT="$(grep -E -w -c '\[ DET \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  #  display_blue "$DET_COUNT" 'DET_COUNT: '
  #
  #  END_COUNT="$(grep -E -w -c '\[ END \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  #  display_blue "$END_COUNT" 'END_COUNT: '
  #
  #  echo
  #
  #  display_blue "$(expr $STR_COUNT + $GEN_COUNT + $TEC_COUNT + $DET_COUNT + $END_COUNT)" 'Total section processed: '
  #
  #  display_blue "$(expr $CHK_MAPPING_FIELDS + \
  #    $CHK_MAPPING_FIELDS_TARGET_ONLY + \
  #    $FAILURE_MAPPING + \
  #    $ERROR_ROW_PATTERN + \
  #    $ERROR_MAPPING_MISSING_SRC_VALUE)" 'Total row of fields mapped: '
  #
  echo
}

export INPUT_DIR=$(prop 'data.sample.input')
export OUTPUT_DIR=$(prop 'data.sample.output')

echo '' >"$QA_CHK_LOG_FULL_PATH"

CURRENT_DIR=$(pwd)
cd "$PROJECT_DIR"
mvn spring-boot:run -Dspring-boot.run.arguments=-m
cd "$CURRENT_DIR"
testMapping_calculation
