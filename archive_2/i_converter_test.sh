#!/usr/bin/bash
# Reset
Color_Off='\033[0m' # Text Reset

# Regular Colors
Black='\033[0;30m'  # Black
Red='\033[0;31m'    # Red
Green='\033[0;32m'  # Green
Yellow='\033[0;33m' # Yellow
Blue='\033[0;34m'   # Blue
Purple='\033[0;35m' # Purple
Cyan='\033[0;36m'   # Cyan
White='\033[0;37m'  # White

PROJECT_DIR=../testware

QA_CHK_LOG_PATH="$PROJECT_DIR"/log/
QA_CHK_LOG_FULL_PATH="$QA_CHK_LOG_PATH"/conversion_quality_check.log

config_file="$PROJECT_DIR"/src/main/resources/config.properties
function prop {
  grep "${1}" ${config_file} | cut -d'=' -f2
}

INPUT_DIR=$(prop '^data.sample.input=')
OUTPUT_DIR=$(prop '^data.sample.output=')

NUMBER_OF_INPUT="$(ls $INPUT_DIR | wc -w)"
NUMBER_OF_OUTPUT="$(ls $OUTPUT_DIR | wc -w)"

ioInfo() {
  echo
  echo -e Processing log...
  echo
  echo -e Number of input "$Blue""$NUMBER_OF_INPUT""$Color_Off" flat file\(s\)...
  echo -e Number of output "$Blue""$NUMBER_OF_OUTPUT""$Color_Off" flat file\(s\)...
}

display_blue() {
  printf "$2%-1s$Blue$1$Color_Off\n"
}

display_green() {
  printf "$2%-1s$Green$1$Color_Off\n"
}

display_yellow() {
  printf "$2%-1s$Yellow$1$Color_Off\n"
}

display_red() {
  printf "$2%-1s$Red$1$Color_Off\n"
}

testFileNameAgainstID() {

  ioInfo

  CHK_FILE_NAME_TO_ID="$(grep -E -i -w 'CHK_FILE_NAME_TO_ID' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  FAILURE_FILE_NAME_TO_ID="$(grep -E -i -w 'FAILURE_FILE_NAME_TO_ID' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  ERROR_FILE_NAME_TO_ID="$(grep -E -i -w 'ERROR_FILE_NAME_TO_ID' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  echo
  echo Testing file names containig its IDs
  echo
  echo -e Number of output'        '"$Blue"$(ls "$OUTPUT_DIR" | wc -w)"$Color_Off"
  echo -e Passed ID in file name:' '"$Green""$CHK_FILE_NAME_TO_ID""$Color_Off"
  echo -e Errors ID in file name:' '"$Yellow""$FAILURE_FILE_NAME_TO_ID""$Color_Off"
  echo -e Errors ID in file name:' '"$Red""$ERROR_FILE_NAME_TO_ID""$Color_Off"
}

echo
display_green For output files ID is in the file name
echo

echo '' > "$QA_CHK_LOG_FULL_PATH"

CURRENT_DIR=$(pwd)
cd "$PROJECT_DIR"
mvn spring-boot:run -Dspring-boot.run.arguments=-i
cd "$CURRENT_DIR"
testFileNameAgainstID
