#!/usr/bin/bash
# Reset
Color_Off='\033[0m' # Text Reset

# Regular Colors
Black='\033[0;30m'  # Black
Red='\033[0;31m'    # Red
Green='\033[0;32m'  # Green
Yellow='\033[0;33m' # Yellow
Blue='\033[0;34m'   # Blue
Purple='\033[0;35m' # Purple
Cyan='\033[0;36m'   # Cyan
White='\033[0;37m'  # White

PROJECT_DIR=../testware

QA_CHK_LOG_PATH="$PROJECT_DIR"/log/
QA_CHK_LOG_FULL_PATH="$QA_CHK_LOG_PATH"/conversion_quality_check.log

config_file="$PROJECT_DIR"/src/main/resources/config.properties
function prop {
  grep "${1}" ${config_file} | cut -d'=' -f2
}

INPUT_DIR=$(prop '^data.sample.input=')
OUTPUT_DIR=$(prop '^data.sample.output=')

NUMBER_OF_INPUT="$(ls $INPUT_DIR | wc -w)"
NUMBER_OF_OUTPUT="$(ls $OUTPUT_DIR | wc -w)"

ioInfo() {
  echo
  echo -e Processing log...
  echo
  echo -e Number of input "$Blue""$NUMBER_OF_INPUT""$Color_Off" flat file\(s\)...
  echo -e Number of output "$Blue""$NUMBER_OF_OUTPUT""$Color_Off" flat file\(s\)...
}

display_blue() {
  printf "$2%-1s$Blue$1$Color_Off\n"
}

display_green() {
  printf "$2%-1s$Green$1$Color_Off\n"
}

display_yellow() {
  printf "$2%-1s$Yellow$1$Color_Off\n"
}

display_red() {
  printf "$2%-1s$Red$1$Color_Off\n"
}

testCorrectFlatFileNames() {

  ioInfo

  CHK_FILE_NAME_FLAT="$(grep -E -i -w -c 'CHK_FILE_NAME_FLAT' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  ERROR_FILE_NAME_PATTERN_FLAT="$(grep -E -i -w -c 'ERROR_FILE_NAME_PATTERN_FLAT' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  echo

  echo Processing output file dir
  echo
  echo -e Number of output'          '"$Blue""$NUMBER_OF_OUTPUT""$Color_Off"
  echo -e Passed file name pattern:' '"$Green""$CHK_FILE_NAME_FLAT""$Color_Off"
  echo -e Errors file name pattern:' '"$Red""$ERROR_FILE_NAME_PATTERN_FLAT""$Color_Off"
}

echo
display_green Correct flat File Name
echo

echo '' >"$QA_CHK_LOG_FULL_PATH"

CURRENT_DIR=$(pwd)
cd "$PROJECT_DIR"
mvn spring-boot:run -Dspring-boot.run.arguments=-f
cd "$CURRENT_DIR"
testCorrectFlatFileNames
