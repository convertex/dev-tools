#!/usr/bin/bash

# Reset
Color_Off='\033[0m' # Text Reset

# Regular Colors
Black='\033[0;30m'  # Black
Red='\033[0;31m'    # Red
Green='\033[0;32m'  # Green
Yellow='\033[0;33m' # Yellow
Blue='\033[0;34m'   # Blue
Purple='\033[0;35m' # Purple
Cyan='\033[0;36m'   # Cyan
White='\033[0;37m'  # White

PROJECT_DIR=../testware

QA_CHK_LOG_PATH="$PROJECT_DIR"/log/
QA_CHK_LOG_FULL_PATH="$QA_CHK_LOG_PATH"/conversion_quality_check.log

config_file="$PROJECT_DIR"/src/main/resources/config.properties
function prop {
  grep "${1}" ${config_file} | cut -d'=' -f2
}

INPUT_DIR=$(prop '^data.sample.input=')
OUTPUT_DIR=$(prop '^data.sample.output=')

NUMBER_OF_INPUT="$(ls $INPUT_DIR | wc -w)"
NUMBER_OF_OUTPUT="$(ls $OUTPUT_DIR | wc -w)"

ioInfo() {
  echo
  echo -e Processing log...
  echo
  echo -e Number of input "$Blue""$NUMBER_OF_INPUT""$Color_Off" flat file\(s\)...
  echo -e Number of output "$Blue""$NUMBER_OF_OUTPUT""$Color_Off" flat file\(s\)...
}

display_blue() {
  printf "$2%-1s$Blue$1$Color_Off\n"
}

display_green() {
  printf "$2%-1s$Green$1$Color_Off\n"
}

display_yellow() {
  printf "$2%-1s$Yellow$1$Color_Off\n"
}

display_red() {
  printf "$2%-1s$Red$1$Color_Off\n"
}

testNumberOfFieldsInOutput() {

  ALL_OUTPUT_FILES=$(ls "$OUTPUT_DIR" | wc -w)

  CHK_FILE="$(grep -E -i -w 'CHK_FILE' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  #  erronous file name
  ERROR_FILE_NAME="$(grep -E -i -w 'ERROR_FILE_NAME' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  # number of fields in the pattern OK rows
  CHK_NUMBER_OF_FIELDS="$(grep -E -i -w 'CHK_NUMBER_OF_FIELDS' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  #  number of rows where the specified number of fields found
  CHK_PASSED="$(grep -E -i -w 'CHK_PASSED' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  # number of rows the expected (based on spec) and the actual number of fields are different
  FAILURE_NUMBER_OF_FIELDS="$(grep -E -i -w 'FAILURE_NUMBER_OF_FIELDS' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  ERROR_ROW_PATTERN="$(grep -E -i -w 'ERROR_ROW_PATTERN' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  ERROR_ROW_MISSING="$(grep -E -i -w 'ERROR_ROW_MISSING' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  ERROR_FIELD_MISSING="$(grep -E -i -w 'ERROR_FIELD_MISSING' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  echo
  echo Report: Checking number of fields in rows...
  echo
  echo -e All files in output:'     '"$Blue""$ALL_OUTPUT_FILES""$Color_Off"
  echo
  echo -e Passed file name pattern: "$Green""$CHK_FILE""$Color_Off"
  echo -e Errors file name pattern: "$Red"$(expr $ALL_OUTPUT_FILES - $CHK_FILE)"$Color_Off"
  echo
  echo Sections processed
  echo
  STR_COUNT="$(grep -E -w -c '\[ STR \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_blue "$STR_COUNT" 'STR_COUNT: '

  GEN_COUNT="$(grep -E -w -c '\[ GEN \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_blue "$GEN_COUNT" 'GEN_COUNT: '

  TEC_COUNT="$(grep -E -w -c '\[ TEC \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_blue "$TEC_COUNT" 'TEC_COUNT: '

  DET_COUNT="$(grep -E -w -c '\[ DET \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_blue "$DET_COUNT" 'DET_COUNT: '

  END_COUNT="$(grep -E -w -c '\[ END \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_blue "$END_COUNT" 'END_COUNT: '

  UNDEF_COUNT="$(grep -E -w -c '\[     \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_blue "$UNDEF_COUNT" '[ ]_COUNT: '
  #

  echo
  display_blue "$(expr $STR_COUNT + $GEN_COUNT + $TEC_COUNT + $DET_COUNT + $END_COUNT + $UNDEF_COUNT)" 'Total section processed: '

  echo
  echo -e Tests passed: "$Green""$CHK_NUMBER_OF_FIELDS""$Color_Off"
  echo -e Failures: "$Yellow""$FAILURE_NUMBER_OF_FIELDS""$Color_Off"
  echo -e Errors full row regexp: "$Red""$ERROR_ROW_PATTERN""$Color_Off"

}

echo
display_green Correct Number Of Fields in the Output Rows
echo

echo '' >"$QA_CHK_LOG_FULL_PATH"

CURRENT_DIR=$(pwd)
cd "$PROJECT_DIR"
mvn spring-boot:run -Dspring-boot.run.arguments=-c
cd "$CURRENT_DIR"
testNumberOfFieldsInOutput
