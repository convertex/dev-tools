#!/usr/bin/bash

# Reset
Color_Off='\033[0m' # Text Reset

# Regular Colors
Black='\033[0;30m'  # Black
Red='\033[0;31m'    # Red
Green='\033[0;32m'  # Green
Yellow='\033[0;33m' # Yellow
Blue='\033[0;34m'   # Blue
Purple='\033[0;35m' # Purple
Cyan='\033[0;36m'   # Cyan
White='\033[0;37m'  # White

PROJECT_DIR=../testware

QA_CHK_LOG_PATH="$PROJECT_DIR"/log/
QA_CHK_LOG_FULL_PATH="$QA_CHK_LOG_PATH"/conversion_quality_check.log

config_file="$PROJECT_DIR"/src/main/resources/config.properties
function prop {
  grep "${1}" ${config_file} | cut -d'=' -f2
}

INPUT_DIR=$(prop '^data.sample.input=')
OUTPUT_DIR=$(prop '^data.sample.output=')

nano "$OUTPUT_DIR"/$1