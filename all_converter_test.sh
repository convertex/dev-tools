#!/usr/bin/bash

# colours of the rainbow (just for fun)

# Reset
Color_Off='\033[0m' # Text Reset

# Regular Colors
Black='\033[0;30m'  # Black
Red='\033[0;31m'    # Red
Green='\033[0;32m'  # Green
Yellow='\033[0;33m' # Yellow
Blue='\033[0;34m'   # Blue
Purple='\033[0;35m' # Purple
Cyan='\033[0;36m'   # Cyan
White='\033[0;37m'  # White

# Bold
BBlack='\033[1;30m'  # Black
BRed='\033[1;31m'    # Red
BGreen='\033[1;32m'  # Green
BYellow='\033[1;33m' # Yellow
BBlue='\033[1;34m'   # Blue
BPurple='\033[1;35m' # Purple
BCyan='\033[1;36m'   # Cyan
BWhite='\033[1;37m'  # White

# Underline
UBlack='\033[4;30m'  # Black
URed='\033[4;31m'    # Red
UGreen='\033[4;32m'  # Green
UYellow='\033[4;33m' # Yellow
UBlue='\033[4;34m'   # Blue
UPurple='\033[4;35m' # Purple
UCyan='\033[4;36m'   # Cyan
UWhite='\033[4;37m'  # White

# Background
On_Black='\033[40m'  # Black
On_Red='\033[41m'    # Red
On_Green='\033[42m'  # Green
On_Yellow='\033[43m' # Yellow
On_Blue='\033[44m'   # Blue
On_Purple='\033[45m' # Purple
On_Cyan='\033[46m'   # Cyan
On_White='\033[47m'  # White

# High Intensity
IBlack='\033[0;90m'  # Black
IRed='\033[0;91m'    # Red
IGreen='\033[0;92m'  # Green
IYellow='\033[0;93m' # Yellow
IBlue='\033[0;94m'   # Blue
IPurple='\033[0;95m' # Purple
ICyan='\033[0;96m'   # Cyan
IWhite='\033[0;97m'  # White

# Bold High Intensity
BIBlack='\033[1;90m'  # Black
BIRed='\033[1;91m'    # Red
BIGreen='\033[1;92m'  # Green
BIYellow='\033[1;93m' # Yellow
BIBlue='\033[1;94m'   # Blue
BIPurple='\033[1;95m' # Purple
BICyan='\033[1;96m'   # Cyan
BIWhite='\033[1;97m'  # White

# High Intensity backgrounds
On_IBlack='\033[0;100m'  # Black
On_IRed='\033[0;101m'    # Red
On_IGreen='\033[0;102m'  # Green
On_IYellow='\033[0;103m' # Yellow
On_IBlue='\033[0;104m'   # Blue
On_IPurple='\033[0;105m' # Purple
On_ICyan='\033[0;106m'   # Cyan
On_IWhite='\033[0;107m'  # White

PROJECT_DIR=../testware

QA_CHK_LOG_PATH="$PROJECT_DIR"/log/
QA_CHK_LOG_FULL_PATH="$QA_CHK_LOG_PATH"/conversion_quality_check.log

config_file="$PROJECT_DIR"/src/main/resources/config.properties
function prop {
  grep "${1}" ${config_file} | cut -d'=' -f2
}

INPUT_DIR=$(prop '^data.sample.input=')
OUTPUT_DIR=$(prop '^data.sample.output=')

NUMBER_OF_INPUT="$(ls $INPUT_DIR | wc -w)"
NUMBER_OF_OUTPUT="$(ls $OUTPUT_DIR | wc -w)"

ioInfo() {
  echo
  echo -e Processing log...
  echo
  echo -e Number of input file "$Blue""$NUMBER_OF_INPUT""$Color_Off"
  echo -e Number of output file "$Blue""$NUMBER_OF_OUTPUT""$Color_Off"
}

display_blue() {
  printf "$2%-1s$Blue$1$Color_Off\n"
}

display_green() {
  printf "$2%-1s$Green$1$Color_Off\n"
}

display_yellow() {
  printf "$2%-1s$Yellow$1$Color_Off\n"
}

display_red() {
  printf "$2%-1s$Red$1$Color_Off\n"
}

testCorrectJzonFileName() {

  ioInfo

  CHK_FILE_NAME_JZON="$(grep -E -i -w -c 'CHK_FILE_NAME_JZON' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  ERROR_FILE_NAME_PATTERN_JZON="$(grep -E -i -w -c 'ERROR_FILE_NAME_PATTERN_JZON' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  echo

  echo Processing "$FUNCNAME"
  echo
  echo -e Number of input:' '"$Blue""$NUMBER_OF_INPUT""$Color_Off"
  echo -e Passed:'          '"$Green""$CHK_FILE_NAME_JZON""$Color_Off"
  echo -e Failures:'        '"$Red""$ERROR_FILE_NAME_PATTERN_JZON""$Color_Off"
}

testCorrectJzonFileExt() {

  ioInfo

  CHK_JZON_FILE_EXT="$(grep -E -i -w -c 'CHK_JZON_FILE_EXT' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  ERROR_FILE_EXT_PATTERN_JZON="$(grep -E -i -w -c 'ERROR_FILE_EXT_PATTERN_JZON' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  echo

  echo Processing "$FUNCNAME"
  echo
  echo -e Number of input:' '"$Blue""$NUMBER_OF_INPUT""$Color_Off"
  echo -e Passed:'          '"$Green""$CHK_JZON_FILE_EXT""$Color_Off"
  echo -e Failures:'        '"$Red""$ERROR_FILE_EXT_PATTERN_JZON""$Color_Off"
}

testCorrectFlatFileName() {

  ioInfo

  CHK_FILE_NAME_FLAT="$(grep -E -i -w -c 'CHK_FILE_NAME_FLAT' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  ERROR_FILE_NAME_PATTERN_FLAT="$(grep -E -i -w -c 'ERROR_FILE_NAME_PATTERN_FLAT' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  echo

  echo Processing "$FUNCNAME"
  echo
  echo -e Number of output:' '"$Blue""$NUMBER_OF_OUTPUT""$Color_Off"
  echo -e Passed:'           '"$Green""$CHK_FILE_NAME_FLAT""$Color_Off"
  echo -e Failures:'         '"$Red""$ERROR_FILE_NAME_PATTERN_FLAT""$Color_Off"
}

testCorrectFlatFileExtension() {

  ioInfo

  CHK_FLAT_FILE_EXT="$(grep -E -i -w -c 'CHK_FLAT_FILE_EXT' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  ERROR_FILE_EXT_PATTERN_FLAT="$(grep -E -i -w -c 'ERROR_FILE_EXT_PATTERN_FLAT' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  echo

  echo Processing "$FUNCNAME"
  echo
  echo -e Number of output:' '"$Blue""$NUMBER_OF_OUTPUT""$Color_Off"
  echo -e Passed:'           '"$Green""$CHK_FLAT_FILE_EXT""$Color_Off"
  echo -e Failures:'         '"$Red""$ERROR_FILE_EXT_PATTERN_FLAT""$Color_Off"
}

testNumberOfFieldsInOutput() {

  ALL_OUTPUT_FILES=$(ls "$OUTPUT_DIR" | wc -w)

  CHK_FILE="$(grep -E -i -w 'CHK_FILE' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  #  erronous file name
  ERROR_FILE_NAME="$(grep -E -i -w 'ERROR_FILE_NAME' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  # number of fields in the pattern OK rows
  CHK_NUMBER_OF_FIELDS="$(grep -E -i -w 'CHK_NUMBER_OF_FIELDS' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  #  number of rows where the specified number of fields found
  CHK_PASSED="$(grep -E -i -w 'CHK_PASSED' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  # number of rows the expected (based on spec) and the actual number of fields are different
  FAILURE_NUMBER_OF_FIELDS="$(grep -E -i -w 'FAILURE_NUMBER_OF_FIELDS' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  ERROR_ROW_PATTERN="$(grep -E -i -w 'ERROR_ROW_PATTERN' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  ERROR_ROW_MISSING="$(grep -E -i -w 'ERROR_ROW_MISSING' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  ERROR_EMPTY_FLAT_LINE="$(grep -E -i -w 'ERROR_EMPTY_FLAT_LINE' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  echo
  echo Processing "$FUNCNAME"
  echo
  echo -e All files in output:'     '"$Blue""$ALL_OUTPUT_FILES""$Color_Off"
  echo
  echo -e Passed file name pattern: "$Green""$CHK_FILE""$Color_Off"
  echo -e Errors file name pattern: "$Red"$(expr $ALL_OUTPUT_FILES - $CHK_FILE)"$Color_Off"
  echo
  echo Sections processed
  echo
  STR_COUNT="$(grep -E -w -c '\[ STR \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_blue "$STR_COUNT" 'STR_COUNT: '

  GEN_COUNT="$(grep -E -w -c '\[ GEN \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_blue "$GEN_COUNT" 'GEN_COUNT: '

  TEC_COUNT="$(grep -E -w -c '\[ TEC \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_blue "$TEC_COUNT" 'TEC_COUNT: '

  DET_COUNT="$(grep -E -w -c '\[ DET \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_blue "$DET_COUNT" 'DET_COUNT: '

  END_COUNT="$(grep -E -w -c '\[ END \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_blue "$END_COUNT" 'END_COUNT: '

  UNDEF_COUNT="$(grep -E -w -c '\[     \]' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_blue "$UNDEF_COUNT" '[ ]_COUNT: '
  #

  echo
  display_blue "$(expr $STR_COUNT + $GEN_COUNT + $TEC_COUNT + $DET_COUNT + $END_COUNT + $UNDEF_COUNT)" 'Total section processed: '

  echo
  echo -e CHK_NUMBER_OF_FIELDS:'     '"$Green""$CHK_NUMBER_OF_FIELDS""$Color_Off"
  echo -e FAILURE_NUMBER_OF_FIELDS:' '"$Yellow""$FAILURE_NUMBER_OF_FIELDS""$Color_Off"
  echo -e ERROR_ROW_PATTERN:'        '"$Red""$ERROR_ROW_PATTERN""$Color_Off"
  echo -e ERROR_EMPTY_FLAT_LINE:'    '"$Red""$ERROR_EMPTY_FLAT_LINE""$Color_Off"

}

testFileNameAgainstID() {

  ioInfo

  CHK_FILE_NAME_TO_ID="$(grep -E -i -w 'CHK_FILE_NAME_TO_ID' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  EXCEPTION_UNMATCHING_ID_AND_FILENAME="$(grep -E -i -w 'EXCEPTION_UNMATCHING_ID_AND_FILENAME' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"
  ERROR_ROW_PATTERN="$(grep -E -i -w 'ERROR_ROW_PATTERN' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l)"

  echo
  echo Processing "$FUNCNAME"
  echo
  echo -e Number of output'                      '"$Blue"$(ls "$OUTPUT_DIR" | wc -w)"$Color_Off"
  echo -e CHK_FILE_NAME_TO_ID:'                  '"$Green""$CHK_FILE_NAME_TO_ID""$Color_Off"
  echo -e EXCEPTION_UNMATCHING_ID_AND_FILENAME:' '"$Yellow""$EXCEPTION_UNMATCHING_ID_AND_FILENAME""$Color_Off"
  echo -e ERROR_ROW_PATTERN:'                    '"$Red""$ERROR_ROW_PATTERN""$Color_Off"
}

testMapping() {

  ioInfo

  echo
  echo Processing "$FUNCNAME"
  echo

  CHK_FILE_NAME_JZON="$(grep -E -w -c 'CHK_FILE_NAME_JZON' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_green "$CHK_FILE_NAME_JZON" 'CHK_FILE_NAME_JZON: '

  ERROR_FILE_NAME_PATTERN_JZON="$(grep -E -w -c 'ERROR_FILE_NAME_PATTERN_JZON' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_red "$ERROR_FILE_NAME_PATTERN_JZON" 'ERROR_FILE_NAME_PATTERN_JZON: '

  CHK_FILE_NAME_FLAT="$(grep -E -w -c 'CHK_FILE_NAME_FLAT' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_green "$CHK_FILE_NAME_FLAT" 'CHK_FILE_NAME_FLAT: '

  ERROR_FILE_NAME_PATTERN_FLAT="$(grep -E -w -c 'ERROR_FILE_NAME_PATTERN_FLAT' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_red "$ERROR_FILE_NAME_PATTERN_FLAT" 'ERROR_FILE_NAME_PATTERN_FLAT: '
  echo
  #
  #  CHK_NUMBER_OF_FIELDS="$(grep -E -w -c 'CHK_NUMBER_OF_FIELDS' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  #  display_green "$CHK_NUMBER_OF_FIELDS" 'CHK_NUMBER_OF_FIELDS: '
  #
  #  FAILURE_NUMBER_OF_FIELDS="$(grep -E -w -c 'FAILURE_NUMBER_OF_FIELDS' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  #  display_yellow "$FAILURE_NUMBER_OF_FIELDS" 'FAILURE_NUMBER_OF_FIELDS: '
  #  echo

  CHK_FILE="$(grep -E -w -c 'CHK_FILE' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_green "$CHK_FILE" 'CHK_FILE: '
  echo

  CHK_MAPPING_FIELDS="$(grep -E -w -c 'CHK_MAPPING_FIELDS' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_green "$CHK_MAPPING_FIELDS" '+ CHK_MAPPING_FIELDS: '

  CHK_MAPPING_FIELDS_USER_TIME_NULL="$(grep -E -w -c 'CHK_MAPPING_FIELDS_USER_TIME_NULL' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_green "$CHK_MAPPING_FIELDS_USER_TIME_NULL" '+ CHK_MAPPING_FIELDS_USER_TIME_NULL: '

  CHK_MAPPING_FIELDS_TARGET_ONLY="$(grep -E -w -c 'CHK_MAPPING_FIELDS_TARGET_ONLY' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_green "$CHK_MAPPING_FIELDS_TARGET_ONLY" '+ CHK_MAPPING_FIELDS_TARGET_ONLY: '

  EXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER="$(grep -E -w -c 'EXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_green "$EXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER" 'D EXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER: '
  echo

  FAILURE_NUMBER_OF_FIELDS="$(grep -E -w -c 'FAILURE_NUMBER_OF_FIELDS' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_yellow "$FAILURE_NUMBER_OF_FIELDS" 'D FAILURE_NUMBER_OF_FIELDS: '

  FAILURE_MAPPING="$(grep -E -w -c 'FAILURE_MAPPING' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_yellow "$FAILURE_MAPPING" '+ FAILURE_MAPPING: '

  FAILURE_MAPPING_FOR_SUBMITTED="$(grep -E -w -c 'FAILURE_MAPPING_FOR_SUBMITTED' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_yellow "$FAILURE_MAPPING_FOR_SUBMITTED" '+ FAILURE_MAPPING_FOR_SUBMITTED: '
  echo

  ERROR_ROW_PATTERN="$(grep -E -w -c 'ERROR_ROW_PATTERN' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_red "$ERROR_ROW_PATTERN" '+ ERROR_ROW_PATTERN: '

  ERROR_PAIRED_WITH_IRREGULAR_FILE_NAME="$(grep -E -w -c 'ERROR_PAIRED_WITH_IRREGULAR_FILE_NAME' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_red "$ERROR_PAIRED_WITH_IRREGULAR_FILE_NAME" '+ ERROR_PAIRED_WITH_IRREGULAR_FILE_NAME: '

  ERROR_EMPTY_FLAT_LINE="$(grep -E -w -c 'ERROR_EMPTY_FLAT_LINE' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_red "$ERROR_EMPTY_FLAT_LINE" 'D ERROR_EMPTY_FLAT_LINE: '

  UNEXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER="$(grep -E -w -c 'UNEXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_red "$UNEXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER" 'D UNEXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER: '
  echo

  ERROR_MAPPING_MISSING_SRC_VALUE="$(grep -E -w -c 'ERROR_MAPPING_MISSING_SRC_VALUE' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_red "$ERROR_MAPPING_MISSING_SRC_VALUE" '+ ERROR_MAPPING_MISSING_SRC_VALUE: '

  ERROR_MAPPING_MISSING_TRG_VALUE="$(grep -E -w -c 'ERROR_MAPPING_MISSING_TRG_VALUE' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_red "$ERROR_MAPPING_MISSING_TRG_VALUE" '+ ERROR_MAPPING_MISSING_TRG_VALUE: '

  ERROR_MAPPING_UNDEFINED="$(grep -E -w -c 'ERROR_MAPPING_UNDEFINED' <<<"$(cat "$QA_CHK_LOG_FULL_PATH")")"
  display_red "$ERROR_MAPPING_UNDEFINED" '+ ERROR_MAPPING_UNDEFINED: '

  echo
  #  CALCULATED="$((13 * $CHK_FILE))"
  #
  #  echo
  #  display_green "$CALCULATED" 'Calculated: '
  #  echo
  #
  #  PROCESSED="$(expr $CHK_MAPPING_FIELDS_USER_TIME_NULL + \
  #    $CHK_MAPPING_FIELDS_TARGET_ONLY + \
  #    $CHK_MAPPING_FIELDS + \
  #    $ERROR_EMPTY_FLAT_LINE + \
  #    $ERROR_PAIRED_WITH_IRREGULAR_FILE_NAME + \
  #    $ERROR_MAPPING_MISSING_SRC_VALUE + \
  #    $ERROR_MAPPING_MISSING_TRG_VALUE + \
  #    $ERROR_MAPPING_UNDEFINED)"
  #
  #  echo
  #  display_green "$CALCULATED" 'Calculated: '
  #  echo
  #
  #  display_blue "$(expr $CALCULATED - $PROCESSED)" '+ Calculated - Processed check = '
  #  echo

}

clear_log() {
  echo '' >"$QA_CHK_LOG_FULL_PATH"
}

echo
PS3="Select test implementation: "
items=("testCorrectJzonFileName"
  "testJzonFileExt"
  "testCorrectFlatFileName"
  "testFlatFileExt"
  "testFileNameAgainstID"
  "testNumberOfFieldsInOutput"
  "testMapping")

select item in "${items[@]}" Quit; do
  echo
  CURRENT_DIR=$(pwd)
  cd "$PROJECT_DIR" || return
  case $REPLY in
  1)
    clear_log
    mvn spring-boot:run -Dspring-boot.run.arguments=-j
    sleep 2
    clear
    testCorrectJzonFileName
    exit
    ;;
  2)
    clear_log
    mvn spring-boot:run -Dspring-boot.run.arguments=-e
    sleep 2
    clear
    testCorrectJzonFileExt
    exit
    ;;
  3)
    clear_log
    mvn spring-boot:run -Dspring-boot.run.arguments=-f
    sleep 2
    clear
    testCorrectFlatFileName
    exit
    ;;
  4)
    clear_log
    mvn spring-boot:run -Dspring-boot.run.arguments=-x
    sleep 2
    clear
    testCorrectFlatFileExtension
    exit
    ;;
  5)
    clear_log
    mvn spring-boot:run -Dspring-boot.run.arguments=-i
    sleep 2
    clear
    testFileNameAgainstID
    exit
    ;;
  6)
    clear_log
    mvn spring-boot:run -Dspring-boot.run.arguments=-c
    sleep 2
    clear
    testNumberOfFieldsInOutput
    exit
    ;;
  7)
    clear_log
    mvn spring-boot:run -Dspring-boot.run.arguments=-m
    sleep 2
    clear
    testMapping
    exit
    ;;
  $((${#items[@]} + 1))) break ;;
  esac
  cd "$CURRENT_DIR" || return
done
