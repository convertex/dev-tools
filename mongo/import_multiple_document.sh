#!/usr/bin/bash

INPUT_DIR_MOCK=../sample_data_mock/input

IMPORT_FILE_NAME_PATTERN="^[0-9]{3}_(GB|XI)[0-9]{12,15}\.json$"

import_multiple_document() {
  for mongo_doc in "$INPUT_DIR_MOCK"/*; do
    file_name=$(echo "$mongo_doc" | cut -d"/" -f4)
    echo "$file_name"
    CHK_FILE_PATTERN=$(echo "$file_name" | grep -E "$IMPORT_FILE_NAME_PATTERN")
    echo "$CHK_FILE_PATTERN"
    if [ ! -z "$CHK_FILE_PATTERN" ]; then
      mongoimport --host=localhost --port=27017 --db hmrc --collection=eorimocks --file "$mongo_doc"
    fi
  done
}

import_multiple_document
