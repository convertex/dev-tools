#!/usr/bin/bash
# Reset
Color_Off='\033[0m' # Text Reset

# Regular Colors
Black='\033[0;30m'  # Black
Red='\033[0;31m'    # Red
Green='\033[0;32m'  # Green
Yellow='\033[0;33m' # Yellow
Blue='\033[0;34m'   # Blue
Purple='\033[0;35m' # Purple
Cyan='\033[0;36m'   # Cyan
White='\033[0;37m'  # White

QA_CHK_LOG_PATH=../testware/log/
QA_CHK_LOG_FULL_PATH="$QA_CHK_LOG_PATH"/conversion_quality_check.log

TESTWARE_DIR=../testware

INPUT_DIR=../sample_data/input_all
OUTPUT_DIR=../sample_data/output_all

#grep -E -i '\[ ' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l
#grep -E -i ' \]' <<<$(cat "$QA_CHK_LOG_FULL_PATH") | wc -l

#
#FLAT_LINE='[ TEC ] [ CHK_MAPPING_FIELDS_TARGET_ONLY ] [ + ] [ EXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER ] '
#
#echo "$FLAT_LINE" | grep -E -i '\]'
#echo "$FLAT_LINE" | grep -E -i '\['

RAW_DATA=raw_data.dat
RAW_DATA_CSV=raw_data.csv
grep -E -i '\[.*\]' <<<$(cat "$QA_CHK_LOG_FULL_PATH") > "$RAW_DATA"

#
cat "$RAW_DATA" | sed 's/\[ + ]//' | sed -E 's/ *\]/;/g' | sed -E 's/\[ *//g' > "$RAW_DATA_CSV"


