#!/usr/bin/bash

# colours of the rainbow (just for fun)

# Reset
Color_Off='\033[0m' # Text Reset

# Regular Colors
Black='\033[0;30m'  # Black
Red='\033[0;31m'    # Red
Green='\033[0;32m'  # Green
Yellow='\033[0;33m' # Yellow
Blue='\033[0;34m'   # Blue
Purple='\033[0;35m' # Purple
Cyan='\033[0;36m'   # Cyan
White='\033[0;37m'  # White

PROJECT_DIR=../testware

SAVE_TO_MOCK_DIR=../sample_data_mock/response

HMRC_ENDPOINT=https://test-api.service.hmrc.gov.uk
EORI_CHECK_URI="${HMRC_ENDPOINT}"/customs/eori/lookup/check-multiple-eori
#
#HMRC_ENDPOINT=http://localhost:5150
#EORI_CHECK_URI=${HMRC_ENDPOINT}/dev/api/releasier/release/eori

EORI_PATTERN_HMRC='^(GB|XI)[0-9]{12,15}$' # based on the response given by SOUAP UI on a bad request
EORI_PATTERN_SPEC='^(GB|XI)[0-9]{12}$'

JZON_FILE_NAME_PATTERN="^[a-zA-Z0-9\-]{11}\.json$"

SERVER_INVALID_PAYLOAD_MESSAGE='Invalid payload'

NR_OF_PASSED=0
NR_OF_FAILED=0
NR_OF_ERRORS=0
PATTERN_ERRORS=0
SERVER_ERROR=0
SKIPPED=0

config_file="$PROJECT_DIR"/src/main/resources/config.properties
function prop {
  grep "${1}" ${config_file} | cut -d'=' -f2
}

INPUT_DIR=$(prop '^data.sample.input=')
NUMBER_OF_INPUT="$(ls $INPUT_DIR | wc -w)"

inputInfo() {
  echo
  echo -e Processing log...
  echo
  echo -e Number of total input "$Blue""$NUMBER_OF_INPUT""$Color_Off"
}

display_report() {
  echo
  echo -e 'Tests run:      ' "$Blue"$(expr $NR_OF_PASSED + $NR_OF_FAILED + $NR_OF_ERRORS)"$Color_Off"
  echo -e 'Tests passed:   ' "$Green""$NR_OF_PASSED""$Color_Off"
  echo -e 'Failures:       ' "$Yellow"$(expr $NR_OF_FAILED)"$Color_Off"
  echo -e 'Errors:         ' "$Red""$NR_OF_ERRORS""$Color_Off"
  echo -e 'Skipped:        ' "$SKIPPED"
  echo -e 'Errors pattern: ' "$Red""$PATTERN_ERRORS""$Color_Off"
}

DOCUMENT_COUNT=0
save_document() {

  if [ ! -z "$1" ]; then

    counter=$(printf "%03d" "$DOCUMENT_COUNT")
    mongo_doc="$counter"_"$1".json

    #    echo Saving "$mongo_doc"

    # echo int o the import file
    echo "$2" >"$SAVE_TO_MOCK_DIR"/"$mongo_doc"

    ((DOCUMENT_COUNT++))
  else
    echo Invalid filename to save
  fi
}

query_endpoint() {

  for entry in "$INPUT_DIR"/*; do

    file_name=$(echo "$entry" | cut -d"/" -f4)
    CHK_FILE_PATTERN=$(echo "$entry" | echo "$file_name" | grep -E "$JZON_FILE_NAME_PATTERN")
    #    echo P $CHK_FILE_PATTERN

    if [ ! -z "$CHK_FILE_PATTERN" ]; then
      CHECK_PATTERN_MSG="$Green"FPOK"$Color_Off"
    else
      CHECK_PATTERN_MSG="$Red"FPER"$Color_Off"
      ((PATTERN_ERRORS++))
    fi

    inputID=$(jq '.[] | .ID' "$entry" | sed 's/"//g')

    inputEORI=$(jq '.[] | .EORI' "$entry" | sed 's/"//g')
    CHK_FILE_EORI_PATTERN=$(echo "$entry" | echo "$inputEORI" | grep -E "$EORI_PATTERN_HMRC")
    if [ ! -z "$CHK_FILE_EORI_PATTERN" ]; then
      CHECK_EORI_MSG="$Green"EPOK"$Color_Off"
    else
      CHECK_EORI_MSG="$Red"EPER"$Color_Off"
      ((PATTERN_ERRORS++))
    fi

    data='{"eoris":['\""$inputEORI"\"']}'

    #    echo "$data"
    #
    #    if [ "$(grep -Ec "$EORI_PATTERN_SPEC" <<<$inputEORI)" -gt 0 ]; then
    #    echo '['
    result=$(curl -s -X POST "$EORI_CHECK_URI" \
      -H 'Accept: application/vnd.hmrc.1.0+json' \
      -H 'Content-Type: application/json' \
      -d "$data")

    grep -n -e "$SERVER_INVALID_PAYLOAD_MESSAGE" <<<"$result" >/dev/null 2>&1
    wellformed_JSON_response=$(echo $?)

    # JSON ready to process
    if [ "$wellformed_JSON_response" -ne 0 ]; then

      #      echo "$result"

      #      no_server_error=$(echo "$result" | jq 2>&1 | grep 'parse error')
      #      if [ ! -z "$no_server_error" ]; then
      #        reason=$(echo "$data" | jq '.SERVER_ERROR')
      #        echo "$reason"
      #        ((SERVER_ERROR++))
      #      fi

      result=$(echo "$result" | jq '.[]')

      eori_error_response=$(echo "$result" | jq '.code')

      #      echo "eori_error_response"
      #
      #      continue

      #      if [ "$eori_error_response" -ne 0 ]; then
      #      fi

      if [ "$1" == "-v" ]; then
        echo "$result"
      fi

      EORI_ID_RESPONSE=$(echo "$result" | jq --raw-output '.eori')
      EORI_VALID_RESP=$(echo "$result" | jq --raw-output '.valid')

      if [ "$EORI_VALID_RESP" == 'true' ]; then
        EORI_VALID_RESP="$Green"PASSED"$Color_Off"
        ((NR_OF_PASSED++))

        save_document "$EORI_ID_RESPONSE" "$result"

      elif [ "$EORI_VALID_RESP" == 'false' ]; then
        EORI_VALID_RESP="$Yellow"FAILED"$Color_Off"
        ((NR_OF_FAILED++))

        save_document "$EORI_ID_RESPONSE" "$result"

      elif [ "$EORI_ERROR_RESPONSE" == 'ture' ]; then
        EORI_ERROR_RESP="$GRed"PASSED"$Color_Off"

        ((NR_OF_ERRORS++))
      else
        echo
        echo Exception occured on "$data"
        #        echo "$result"
        echo
        echo "[      ]" "$file_name" "$data"
        EORI_ID_RESPONSE="              "
        EORI_VALID_RESP="$Red"ERROR"$Color_Off"
      fi
    else
      echo
      echo "[      ]" "$file_name" "$data"
      EORI_ID_RESPONSE="              "
      EORI_VALID_RESP="$Red"ERROR"$Color_Off"
      ((NR_OF_ERRORS++))
    fi

    #  file name pattern, eori pattern
    #  Input.[0].ID
    #  Resp.[0].id
    #  Resp.[0].valid
    echo -e \[ "$CHECK_PATTERN_MSG" \] \
      \[ "$CHECK_EORI_MSG" \] \
      \[ "$inputID" \] \
      \[ "$EORI_ID_RESPONSE" \] \
      "$EORI_VALID_RESP"

  done

}

clear_mock_downloads() {
  rm -f "$SAVE_TO_MOCK_DIR"/*
}

_test() {
  jzon='{
         "code": "INVALID_REQUEST",
         "message": "Invalid MOCK eori - eori parameters should be between 12 or 15 digits"
       }'

  echo "$jzon" | jq '.code'
  exit
}

clear_mock_downloads
if [ ! -z "$1" ]; then
  VERBOSE="-v"
else
  VERBOSE=
fi

inputInfo

query_endpoint "$VERBOSE"
display_report
